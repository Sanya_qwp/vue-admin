import Vue       from 'vue'
import axios     from 'axios'
import VueAxios  from 'vue-axios'
import Vuex      from 'vuex'
import VueRouter from 'vue-router'
import 'normalize.css';
// import 'bootstrap';
// import 'aws-sdk';
// import 'childprocess';
// import 'child_process';
// import 'popper';
// import 'jquery';
import App from './App'

import { router } from './router/router'
import { store } from './store/store'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)

export const bus = new Vue();

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})