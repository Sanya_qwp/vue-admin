import Vue       from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Main from '../pages/Main'
import Auth from '../pages/Auth'
import ComponentsList from '../pages/ComponentsList'
import AddPage from '../pages/AddPage'
import RenamePage from '../pages/RenamePage'
import ColorTheme from '../pages/ColorTheme'

export const router = new VueRouter({
  base: '/',
  mode: 'history',
  routes:[
    {
      path: '/',
      name: '',
      component: Main
    },
    {
      path: '/Componentslist',
      name: '',
      component: ComponentsList
    },
    {
      path: '/auth',
      name: '',
      component: Auth
    },
    {
      path: '/add-page',
      name: '',
      component: AddPage
    },
    {
      path: '/rename-page',
      name: '',
      component: RenamePage
    },
    {
      path: '/color-theme',
      name: '',
      component: ColorTheme
    }
  ]
})
